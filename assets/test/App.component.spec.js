import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';

import App from '../js/components/App'
import HelloWorld from '../js/containers/HelloWorld'

function setup(testProps) {
    const enzymeWrapper = shallow(<App />)

    return {
        enzymeWrapper
    }
}

describe('<App /> component', () => {
    it('should render 1 <h1>', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.find('h1')).to.have.length(1);
    });

    it('should render 1 <HelloWorld> container', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.find(HelloWorld)).to.have.length(1);
    });

    it('should render 1 <HelloWorld> with a message prop set', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.find(HelloWorld).props().message).to.be.a('string');
    });
});