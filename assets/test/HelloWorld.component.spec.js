import React from 'react';
import { mount } from 'enzyme';
import { expect, assert } from 'chai';
import sinon from 'sinon';

import HelloWorld from '../js/components/HelloWorld'

function setup(testProps) {
    const props = {
        storeMessage: {},
        onClick: null,
        ...testProps
    }

    // make prop errors throw an error
    const error = console.error;
    console.error = function(warning, ...args) {
        if (/(Invalid prop|Failed prop type)/.test(warning)) {
            throw new Error(warning)
        }
        error.apply(console, [warning, ...args])
    }

    const enzymeWrapper = mount(<HelloWorld {...props} />)

    return {
        props,
        enzymeWrapper
    }
}

describe('<HelloWorld /> component', () => {
    it('should render <h1> <h2> & <p>', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.find('h1')).to.have.length(1);
        expect(enzymeWrapper.find('h2')).to.have.length(1);
        expect(enzymeWrapper.find('p')).to.have.length(1);
    });
    
    it('should render a `.hello-world`', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.find('.hello-world')).to.have.length(1);
    });

    it('should set a message from prop', function () {
        const message = "test message"
        const { enzymeWrapper } = setup({message})
        expect(enzymeWrapper.props().message).to.be.a('string')
        expect(enzymeWrapper.props().message).to.equal(message)
    });

    it('should trigger method on button click', () => {
        const onClick = sinon.spy();
        const { enzymeWrapper } = setup({onClick})
        enzymeWrapper.find('button').simulate('click')
        expect(onClick.calledOnce).to.equal(true)
    });

    it('should have a default message', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.props().message).to.be.a('string')
    })
    it('should throw error if no storeMessage', function () {
        assert.throws(function(){
            setup({ storeMessage: null })
        }, Error)
    })
});