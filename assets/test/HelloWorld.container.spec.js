import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import { Provider } from 'react-redux'

import store from '../js/store'
import HelloWorld from '../js/containers/HelloWorld'

function setup(testProps) {
    const props = {
        message: "Initial message",
        ...testProps
    }

    const enzymeWrapper = mount(
        <Provider store={store}>
            <HelloWorld {...props} />
        </Provider>
    )

    return {
        props,
        enzymeWrapper
    }
}

describe('<HelloWorld /> container', () => {
    it('should render <h1> <h2> & <p>', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.find('h1')).to.have.length(1);
        expect(enzymeWrapper.find('h2')).to.have.length(1);
        expect(enzymeWrapper.find('p')).to.have.length(1);
    });
    it('should update message on button click', () => {
        const { enzymeWrapper } = setup()
        let h2 = enzymeWrapper.find(HelloWorld).find('h2')

        const initialMessage = h2.text()
        expect(initialMessage).to.be.a('string')

        enzymeWrapper.find('button').simulate('click')
        const updatedMessage = h2.text()
        expect(initialMessage).to.not.equal(updatedMessage)
        expect(initialMessage).to.be.a('string')
    });
});