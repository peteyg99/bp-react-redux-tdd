import React from 'react';
import { expect } from 'chai';

import reducer from '../js/reducers'
import { UPDATE_MESSAGE } from '../js/constants'

function performAction(action) {
    const initialState = {
        storeMessage: {
            message: "Initial message"
        }    
    }
    return reducer(initialState, action)
}

describe('StoreMessageReducer', () => {

    it('should update message', function () {
        const action = {
            type: UPDATE_MESSAGE,
            newStoreMessage: "New message"
        }
        const updatedState = performAction(action)

        expect(updatedState.storeMessage.message).to.equal(action.newStoreMessage)
    });

});