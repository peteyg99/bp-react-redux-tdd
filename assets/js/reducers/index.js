import { combineReducers } from 'redux'

// Reducers
import StoreMessageReducer from './StoreMessageReducer'

// Combine Reducers
export default combineReducers({
        storeMessage: StoreMessageReducer,
})
