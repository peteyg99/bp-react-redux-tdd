import { Server } from 'http'
import Express from 'express'
import React from 'react'
import { renderToString } from 'react-dom/server'
import { Provider } from 'react-redux'
import store from './store'
import App from './components/App'

// initialize the server and configure support for ejs templates
const app = new Express()
const server = new Server(app)

// configure support for ejs templates
app.set('view engine', 'ejs')

// set views folder
app.set('views', './assets/views')
app.use(Express.static('./public'))

// routing
app.get('*', (req, res) => {
    const markup = renderToString(
            <Provider store={store}>
                <App/>
            </Provider>
    )
    return res.render('index', { 
          markup
      })
})

// start the server
const port = process.env.PORT || 3000
const env = process.env.NODE_ENV || 'production'
server.listen(port, err => {
  if (err) {
    return console.error(err)
  }
  console.info(`Server running on http://localhost:${port} [${env}]`)
})
