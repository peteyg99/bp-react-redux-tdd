import React, { PropTypes } from 'react'

class HelloWorld extends React.Component {
    constructor(props) {
        super(props)
        this._onClick = this._onClick.bind(this)
    }
    
    _onClick() {
        const newMessage = "Yes I am :)"
        this.props.onClick(newMessage)
    }

    render(){
        return (
            <div className="hello-world">
                <h1>Hello {this.props.message}!</h1>
                <h2>{this.props.storeMessage.message}</h2>
                <p>
                    <button type="button" onClick={this._onClick}>Try me</button>
                </p>
            </div>
        )
    }
}

HelloWorld.defaultProps = {
  message: "default world"
};

HelloWorld.propTypes = {
  message: PropTypes.string.isRequired,
  storeMessage: PropTypes.object.isRequired,
}

export default HelloWorld