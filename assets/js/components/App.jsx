import React from 'react'
import HelloWorld from '../containers/HelloWorld'

export default class App extends React.Component {
    render(){
        return (
            <div>
                <h1>BoilerPlate - React</h1>
                <HelloWorld message="world" />
            </div>
        )
    }
}