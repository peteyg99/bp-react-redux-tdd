import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import reducer from './reducers'

const blankState = {
    storeMessage: {
        message: "Am I working?"
    }
}

const initialState = Object.assign({}, blankState)

let middleware = [thunk]

if (process.env.NODE_ENV !== 'production') {
    let logger = createLogger({
        collapsed: true,
        colors: {
            title: () => false,
            prevState: () => `#9E9E9E`,
            action: () => `#03A9F4`,
            nextState: () => `#4CAF50`,
            error: () => `#F20404`
        }
    })

    middleware = [...middleware, logger]
}

const store = createStore(reducer, initialState, applyMiddleware(...middleware))

export default store