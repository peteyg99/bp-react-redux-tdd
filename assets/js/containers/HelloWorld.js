import { connect } from 'react-redux'
import { updateStoreMessage } from '../actions'
import View from '../components/HelloWorld'

const mapStateToProps = (state, ownProps) => {
    return {
        storeMessage: state.storeMessage
    }    
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {dispatch} = dispatchProps
    const {storeMessage} = stateProps

    return {
        storeMessage,
        message: ownProps.message,
        onClick: (newStoreMessage) => {
            dispatch(updateStoreMessage(newStoreMessage))
        }
    }
}

const HelloWorld = connect(
  mapStateToProps,
  null,
  mergeProps
)(View)

export default HelloWorld