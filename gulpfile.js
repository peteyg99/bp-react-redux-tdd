'use strict'

var gulp = require('gulp'),
    del = require('del'),
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    react = require('gulp-react'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream')
 
process.env.NODE_ENV = 'production'
 
var browserifyConfig = {
    extensions: ['.js', '.jsx'],
    debug: !isProduction(),
}

var browserifySvrConfig = {
    extensions: ['.js', '.jsx'],
    debug: !isProduction(),
    bare: true,
    browserField: false,
    insertGlobalVars: {
        process: function() { return; }
    },
    builtins: false,
    commondir: false
}

var config = {
    lessSrcFile: 'site',
    lessSrcPath: './assets/less/',
    lessOutPath: './public/css/',

    jsSrcPath: './assets/js/',
    client: {
        jsFile: 'client',
        jsOutPath: './public/js/',
        jsSrcFileExt: '.js',
        browserify: browserifyConfig
    },
    server: {
        jsFile: 'server',
        jsOutPath: './private/',
        jsSrcFileExt: '.js',
        browserify: browserifySvrConfig
    }
}
config.lessSrcFilePath = config.lessSrcPath + config.lessSrcFile + '.less'
config.cleanLessPath = config.lessOutPath + config.lessSrcFile + '.*'

config.client.jsSrcPath = 
    config.jsSrcPath + 
    config.client.jsFile + 
    config.client.jsSrcFileExt

config.server.jsSrcPath = 
    config.jsSrcPath + 
    config.server.jsFile + 
    config.server.jsSrcFileExt


// Grouped tasks

gulp.task('default', ['js:client', 'js:server', 'less'])
gulp.task('dev', ['set-dev', 'default'])


// Single tasks

gulp.task('set-dev', function() {
    return setDev()
})
gulp.task('js:client', function() {
    return bundleJs(config.client)
})
gulp.task('js:server', function() {
    return bundleJs(config.server)
})
gulp.task('less', function() {
    return bundleLess()
})


// Methods

function setDev() {
    process.env.NODE_ENV = 'development'
    gulp.watch(config.lessSrcPath + '**/*.less', ['less'])
    gulp.watch(config.jsSrcPath + '**/*.js*', ['js:client'])
}

function bundleLess() {
    del(config.cleanLessPath)

    return gulp.src(config.lessSrcFilePath)
            .pipe(less())
            .pipe(gulp.dest(config.lessOutPath))
}

function bundleJs(srcType) {
    del(srcType.jsOutPath + srcType.jsFile + '.*')

    return browserify(
        srcType.jsSrcPath, 
        srcType.browserify
    )
    .transform(babelify, {
        presets: ['es2015', 'react'],
        plugins: ['transform-object-rest-spread']
    })
    .bundle()
    .on("error", function (err) { 
        console.log("Error : " + err.message) 
        if (!isProduction()) this.emit('end')
    })
    .pipe(source(srcType.jsFile + srcType.jsSrcFileExt))
    .pipe(rename({ extname: '.js' }))
    .pipe(gulp.dest(srcType.jsOutPath))
}

function isProduction(){
    return process.env.NODE_ENV === 'production'
}